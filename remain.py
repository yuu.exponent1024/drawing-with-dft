from presentation import *
import numpy as np
import matplotlib.pyplot as plt
import PySimpleGUI as sg
from PIL import Image,ImageDraw,ImageOps
import io
from glob import glob
from random import randint

isDrawing = False
isPainting = False
is_animated = False
dt = lastx = lasty = idx = 0
quality = 300
doAnimation = False
prev = None
filename = "back.png"
white = (255,255,255)
draw_color = "white"
wave_color = "black"
app_size = (840,920)
draw_canvas_size = (400,400)
draw_thickness = 2
im = Image.new("RGB",draw_canvas_size,white)
draw = ImageDraw.Draw(im)
seq_X,seq_Y = [],[]
x_center = (200,200)
y_center = (200,200)
A_list = []
index = 0

sg.theme("Black")

def get_img_data(f, maxsize=(533,400), first=False):
    """Generate image data using PIL
    """
    #print("open file:", f)
    img = Image.open(f)
    img.thumbnail(maxsize)
    if first:  # tkinter is inactive the first time
        bio = io.BytesIO()
        img.save(bio, format="PNG")
        del img
        return bio.getvalue()
    return ImageTk.PhotoImage(img)

Animation_Image = sg.Image(data=get_img_data(filename, first=True))

layout = [
    [sg.MenuBar([
                    ["&File",
                        ["New","Open","Paint","&Save as","Close","---","&Quit"]
                    ],
                    ["&Process",
                        ["Calculate ||","Pause ▶","Animation"]
                    ],
                ])
    ],
    [
        sg.Text("Pen Color         :"),sg.Input(key='Color',default_text="#000000"), sg.ColorChooserButton('ColorButton', target='Color')
    ],
    [
        sg.Text("Pen Thickness :"),sg.Slider(range=(1,100),key='Thickness',default_value=2,orientation='h',)
    ],
    [sg.HorizontalSeparator()],
    [
        sg.Graph(
            canvas_size = draw_canvas_size,
            graph_bottom_left = (0,400),
            graph_top_right = (400,0),
            key = "graph",
            change_submits = True,
            background_color = draw_color,
            drag_submits = True
        ),
        sg.Graph(
            canvas_size = draw_canvas_size,
            graph_bottom_left = (0,400),
            graph_top_right = (400,0),
            key = "wave_x",
            change_submits = True,
            background_color = wave_color,
            drag_submits = True
        )
    ],
    [
        sg.Graph(
            canvas_size = draw_canvas_size,
            graph_bottom_left = (0,400),
            graph_top_right = (400,0),
            key = "wave_y",
            change_submits = True,
            background_color = wave_color,
            drag_submits = True
        ),
        Animation_Image
    ]
]

window = sg.Window("Drawing with DFT",layout,size = app_size,location = (50,50),icon=r"wave.ico")
window.Finalize()

graph = window.Element("graph")
wave_x = window.Element("wave_x")
wave_y = window.Element("wave_y")
draw.rectangle((0,0, draw_canvas_size), fill=white, outline=white)

while True:
    e,v = window.read(timeout = (1/60)*100,timeout_key = "Timeout")
    if e is None:
        print("exit")
        break
    if e.startswith("graph"):
        x,y = v["graph"]
        if not e.endswith("+UP"):
            #一度でも書いたらTrue
            if not isDrawing:
                isDrawing = True
                seq_X.append(x)
                seq_Y.append(y)
            if not isPainting:
                graph.DrawCircle(v["graph"],draw_thickness,fill_color="black",line_color="black")

                # 画像に描く、canvasと原点座標のｘが左端と右端なので画像サイズから引く
                draw.ellipse((x-draw_thickness,y-draw_thickness,x+draw_thickness,y+draw_thickness), 
                          fill=(0, 0, 0), outline=(0, 0, 0))
                #黒塗り
                wave_x.Erase()  # グラフを一回削除
                wave_y.Erase()

                for i in range(len(seq_X)):
                    if i == 0:
                        x1,y1 = x,y
                    else:
                        x1,y1 = seq_X[-i],seq_Y[-i]
                    wave_x.DrawCircle((i,y1),draw_thickness,fill_color="green",line_color="green")
                    wave_y.DrawCircle((x1,i),draw_thickness,fill_color="green",line_color="green")
                    if i > 0:
                        if i == 1:
                            x0,y0 = x,y
                        else:
                            x0,y0 = seq_X[-(i-1)],seq_Y[-(i-1)]
                        wave_x.DrawLine(((i-1),y1),(i,y0),width=2*draw_thickness,color = "green")
                        wave_y.DrawLine((x1,(i-1)),(x0,i),width=2*draw_thickness,color = "green")
            
                #点を描画
                if len(seq_X) > 0:
                    x0,y0 = seq_X[-1],seq_Y[-1]
                    #print("(%d %d %d %d)" % (x0,y0,x,y))
                    graph.DrawLine((x0,y0),(x,y),width=2*draw_thickness)
                    draw.line((x0,y0,x,y),fill=(0,0,0),width=2*draw_thickness)
                if seq_X[-1] != x and seq_Y[-1] != y:
                    seq_X.append(x)
                    seq_Y.append(y)
            else:
                color,thickness = v["Color"],int(v["Thickness"])
                color_tuple = (int(color[1:3], 16),int(color[3:5], 16),int(color[5:7], 16))
                #print(color_tuple,color,thickness)

                graph.DrawCircle(v["graph"],thickness,fill_color=color,line_color=color)
                # 画像に描く、canvasと原点座標のｘが左端と右端なので画像サイズから引く
                draw.ellipse((x-thickness,y-thickness,x+thickness,y+thickness), fill=color_tuple, outline=color_tuple)

    elif e == "Close":
        r = sg.PopupOKCancel("一度Closeすると今まで書いた作品が消えてしまいますが、宜しいですか?",title = "Drawing with DFT")
        #print(r)
        if r == "OK":
            graph.Erase()
            wave_x.Erase()
            wave_y.Erase()
            Animation_Image.update(data=get_img_data("back.png", first=True))
            draw.rectangle((0,0, draw_canvas_size), fill=white, outline=white)
            seq_X,seq_Y = [],[]
            if isDrawing:
                isDrawing = False
        else:
            print("Close operation is suspended")
    elif e == "Quit":
        if isDrawing:
            r = sg.Popup("終了するにはまずCloseする必要があります。",title = "Drawing with DFT")
        else:
            print("Drawing with DFTプログラムを停止します。")
            break
        
    elif e == "Save as":
        filename = sg.popup_get_file('save', save_as=True)
        print(filename)
        im_invert = ImageOps.invert(im.convert("RGB"))
        #im.putalpha(im_invert.convert("L"))
        try:
            im.save(filename)
            Animation_Image.update(data=get_img_data(filename, first=True))
        except ValueError:
            print("cancel is selected.")
    elif e == "Calculate ||":
        order = 60 # We need higher order approximation to get better approximation
        space = np.linspace(0, tau, 300) 
        dt = 2*np.pi /(len(seq_X)-1)
        seq_T = [0]
        for i in range(len(seq_X)-1):
            seq_T.append(seq_T[-1] + dt)
        
        if len(seq_X) > 0:
            coef = coef_list(np.array(seq_T),np.array(seq_X),np.array(seq_Y), order)
            x_DFT = [DFT(t, coef, order)[0] for t in space]
            y_DFT = [DFT(t, coef, order)[1] for t in space]
            prev = (x_DFT[0],y_DFT[0])
            graph.DrawCircle(prev,draw_thickness,fill_color="red",line_color="red")
        
            for i in range(1,len(x_DFT)):
                x0,y0 = prev
                x,y = x_DFT[i],y_DFT[i]
                graph.DrawLine((x0,y0),(x,y),width=2*draw_thickness,color="red")
                graph.DrawCircle((x,y),draw_thickness,fill_color="red",line_color="red")
                prev = (x,y)
        else:
            print("x,y座標が記録されていないため描画できません")

        if filename != "back.png":
            time_table, x_table, y_table = create_close_loop(filename)
            if len(seq_X) <= 0:
                window.FindElement('graph').DrawImage(data=get_img_data("contour.png",first=True), location=(0,0))
            coef1 = coef_list(time_table,x_table, y_table, order)
            x_DFT1 = [DFT(t, coef1, order)[0] for t in space]
            y_DFT1 = [DFT(t, coef1, order)[1] for t in space]

            fig, ax = plt.subplots(figsize=(5,5))
            ax.plot(x_DFT1, y_DFT1, 'r--')
            ax.plot(x_table, y_table, 'k-')
            ax.set_aspect('equal', 'datalim')
            xmin, xmax = plt.xlim()
            ymin, ymax = plt.ylim()
            anim = visualize(x_DFT1, y_DFT1, coef1, order, space, [xmin, xmax, ymin, ymax])
            Writer = animation.writers['html']
            writer = Writer(fps=60)
            anim.save('ffmpeg.html',writer=writer, dpi=150)
        else:
            print("絵が保存されていないため描画できません")
    elif e == "Animation":
        index = 0
        A_list = []
        A_list.append("back.png")
        doAnimation = True
    elif e == "Pause ▶":
        doAnimation = not doAnimation 
    elif e == "Timeout" and doAnimation:
        if index == 0:
            for f in glob("ffmpeg_frames" + "/*.png"):
                A_list.append(f)
            index += 1
        elif index > 0 and index <= 300:
            Animation_Image.update(data=get_img_data(A_list[index], first=True))
            print(index)
            index += 1
        else:
            doAnimation = False    
    elif e == "Paint":
        isPainting = not isPainting
        prev = None
    elif e == "Open":
        filename = sg.popup_get_file('open', save_as=False)
        print(filename)
        window.FindElement('graph').DrawImage(data=get_img_data(filename,first=True), location=(0,0))
        im = Image.open(filename)
    else:
        if e != "Timeout":
            print(e,v)

window.close()
